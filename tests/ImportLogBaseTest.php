<?php
/**
 * Created by PhpStorm.
 * User: Sergey
 * Date: 15.03.2019
 * Time: 18:42
 */

namespace tests;

use Symfony\Component\Dotenv\Dotenv;
use PHPUnit\Framework\TestCase;

$env_file = __DIR__ . '/../.env';
if (file_exists($env_file)) {
    (new Dotenv())->load($env_file);
}

class ImportLogBaseTest extends TestCase
{
    public static function token()
    {
        return getenv('YANDEX_API_TOKEN');
    }

    public function testImportLogBase401Unauthorized(): void
    {
        $import = new \ImportLogBase('wrong-token');
        $this->expectException(\Exception::class);
        $import->getStats();
    }

    public function testImportLogBaseStatsCompare(): void
    {
        $diff = [];
        $import = new \ImportLogBase(self::token());
        $stats = $import->getStats();

        //var_dump($stats);

        if (is_array($stats)) {
            if (count($stats) > 0) {
                foreach ($stats as $stat) {
                    $diff = array_merge($diff, array_diff_key(
                        json_decode('{
    "url" : "http://site.ru/3.xml",
    "total" : 33,
    "accepted" : 16,
    "declined" : 17
  }', true),
                        $stat
                    ));

                    // errors может отсутствовать
                    if (isset($stat['errors']) && count($stat['errors']) > 0) {
                        $diff = array_merge($diff, array_diff_key(
                            json_decode('{
        "type" : "Неизвестно местоположение объекта",
        "description" : "Проверьте правильность передаваемых данных в элементе location. См. «Общая информация об объявлении» для жилой недвижимости, для коммерческой недвижимости и для новостроек.",
        "count" : 1
      }', true),
                            $stat['errors'][0]
                        ));
                    }
                }
                $this->assertTrue(count($diff) == 0);
                return;
            }
        }

        $this->assertTrue(is_array($stats));
    }

    public function testImportLogBaseLinksCompare(): void
    {
        $diff = [];
        $import = new \ImportLogBase(self::token());
        $links = $import->getLinks();

        //var_dump($links);

        if (is_array($links)) {
            if (count($links) > 0) {
                foreach ($links as $link) {
                    $diff = array_merge($diff, array_diff_key(
                        json_decode('{
    "id" : 123456,
    "url" : "https://...."
  }', true),
                        $link
                    ));

                    // errors может отсутствовать
                    if (isset($link['errors']) && count($link['errors']) > 0) {
                        $diff = array_merge($diff, array_diff_key(
                            json_decode('{
        "type" : "Неизвестно местоположение объекта",
        "description" : "Проверьте правильность передаваемых данных в элементе location. См. «Общая информация об объявлении» для жилой недвижимости, для коммерческой недвижимости и для новостроек."
      }', true),
                            $link['errors'][0]
                        ));
                    }
                }
                $this->assertTrue(count($diff) == 0);
                return;
            }
        }

        $this->assertTrue(is_array($links));
    }

    public function testImportLogBaseShowsCompare(): void
    {
        $import = new \ImportLogBase(self::token());

        $shows = false;
        $links = $import->getLinks(1);
        foreach ($links as $link) {
            $shows = $import->getShows($link['id']);
        }

        //var_dump($shows);

        if (is_array($shows)) {
            if (count($shows) > 0) {
                $show = $shows[0];
                $diff = array_diff_key(
                    json_decode('{"date":"2019-03-12", "shows":15, "card_shows":15,"calls":7}', true),
                    $show
                );
                $this->assertTrue(count($diff) == 0);
                return;
            }
        }

        $this->assertTrue(is_array($shows));
    }
}
